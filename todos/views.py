from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList
from todos.forms import TodoForm
# Create your views here.


def todos_list(request):
    name_list = TodoList.objects.all()
    context = {
        "name_list": name_list,
    }
    return render(request, "todos/todos.html", context)


def todo_list_detail(request, id):
    list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "list_detail": list_detail,
    }
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todos_list")
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def update_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_list.id)

    else:
        form = TodoForm(instance=todo_list)
        context = {
            "form": form,
            "todo_list": todo_list,
        }
    return render(request, "todos/update_list.html", context)


def delete_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        todo_list.delete()
        return redirect("todos_list")
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/delete.html", context)
