from django.urls import path
from todos.views import todos_list, todo_list_detail, create_list, update_list, delete_list

urlpatterns = [
    path("", todos_list, name="todos_list_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", create_list, name="create_list"),
    path("<int:id>/update/", update_list, name="update_list"),
    path("<int:id>/delete/", delete_list, name="delete_list"),
]
